
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IbanValidationApproach {

    private static final String TAG = IbanValidationApproach.class.getSimpleName();

    private IbanInput ibanInput;

    IbanValidationApproach(IbanInput ibanInput) {
        this.ibanInput = ibanInput;
    }

    public void start(){
        System.out.println("Choose IBAN validation method.");
        System.out.println("Type 1 - manual validation.");
        System.out.println("Type 2 - validation from a file.");
        try(InputStreamReader in = new InputStreamReader(System.in)) {
            BufferedReader br = new BufferedReader(in);
            String iban = br.readLine();
            switch (iban){
                case "1":
                    ibanInput.validateIbanInput();
                    break;
                case "2":
                    ibanInput.validateIbanFileInput();
                    break;
                default:
                    System.out.println("Wrong input, try again.");
                    start();
                    break;
            }
        } catch (IOException e) {
            System.err.println(TAG + e.getMessage());
        }
    }
}
