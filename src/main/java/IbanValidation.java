import iban.FileUtil;
import iban.IbanValidationUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class IbanValidation implements IbanInput {

    private static final String TAG = IbanValidation.class.getSimpleName();

    IbanValidation() {
    }

    public void start(){
        new IbanValidationApproach(this).start();
    }

    @Override
    public void validateIbanInput() {
        System.out.println("Enter account number :");
        try(InputStreamReader in = new InputStreamReader(System.in)) {
            BufferedReader br = new BufferedReader(in);
            String iban = br.readLine();
            boolean result = IbanValidationUtil.validateIban(iban);
            System.out.println("Result = " + result);
        } catch (IOException e) {
            System.err.print(TAG + " Failure in validating IBAN " + e);
        }
    }

    @Override
    public void validateIbanFileInput() {
        System.out.println("Enter file location :");
        try(InputStreamReader in = new InputStreamReader(System.in)) {
            BufferedReader br = new BufferedReader(in);
            File file = new File(br.readLine());
            if (file.exists()) {
                List<String> validationResults = new ArrayList<>();
                Files.lines(file.toPath())
                        .forEach(line -> validationResults.add(line + ";" + IbanValidationUtil.validateIban(line)));
                FileUtil.writeListToFile(validationResults, file.getPath());
                System.out.println("Success.");
            } else {
                System.out.println("File does not exist, try again.");
                validateIbanFileInput();
            }
        } catch (IOException e) {
            System.err.print(TAG + " Failure in validating IBAN " + e);
        }
    }
}
