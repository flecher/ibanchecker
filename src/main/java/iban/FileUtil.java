package iban;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileUtil {

    private static final String TAG = FileUtil.class.getSimpleName();

    private FileUtil() {
    }

    public static void writeListToFile(List<String> list, String filePath) {

        StringBuilder builder = new StringBuilder();
        String filename = filePath + ".out";

        list.forEach(item -> builder.append(item).append("\r\n"));

        try(FileWriter fw = new FileWriter(filename, true)){
            fw.write(builder.toString());
        }catch (IOException e){
            System.err.println(TAG + e.getMessage());
        }
    }
}
