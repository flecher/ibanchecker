package iban;

import com.sun.istack.internal.NotNull;

import java.math.BigInteger;

public class IbanValidationUtil {

    private static final int IBAN_MIN_LENGHT = 15;
    private static final int IBAN_MAX_LENGHT = 34;
    private static final BigInteger IBAN_MAGIC_NUMBER = new BigInteger("97");

    private IbanValidationUtil() {
    }

    public static boolean validateIban(@NotNull String iban) {
        String ibanTrimmed = iban.trim();

        if (ibanTrimmed.length() < IBAN_MIN_LENGHT || ibanTrimmed.length() > IBAN_MAX_LENGHT) {
            return false;
        }

        ibanTrimmed = ibanTrimmed.substring(4) + ibanTrimmed.substring(0, 4);
        StringBuilder numericAccountNumber = new StringBuilder();
        for (int i = 0; i < ibanTrimmed.length(); i++) {
            numericAccountNumber.append(Character.getNumericValue(ibanTrimmed.charAt(i)));
        }

        BigInteger ibanNumber = new BigInteger(numericAccountNumber.toString());
        return ibanNumber.mod(IBAN_MAGIC_NUMBER).intValue() == 1;
    }
}
