import iban.IbanValidationUtil;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IbanValidationUtilTest {

    @Test
    public void validateTooShortIban(){
        String ibanMock = "AA0512454";
        boolean result = IbanValidationUtil.validateIban(ibanMock);
        assertFalse(result);
    }

    @Test
    public void validateTooLongIban(){
        String ibanMock = "AA051245445454552117989";
        boolean actual = IbanValidationUtil.validateIban(ibanMock);
        assertFalse(actual);
    }

    @Test
    public void validateCorrectIban(){
        String ibanMock = "LT647044001231465456";
        boolean actual = IbanValidationUtil.validateIban(ibanMock);
        assertTrue(actual);
    }
}
