# IBAN Checker #

### What you’ll need: ###

* IDE (prefered IntelliJ IDEA)

* Java JDK 1.8

* Gradle (preferd 4.12 version)

	

### Running the app: ###

* Download this project and open with your IDE.

* To run this app, find Main.class file and run "main" method as console application.